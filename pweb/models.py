from django.db import models


# Create your models here.


class User(models.Model):
    uid = models.CharField(max_length=256)
    type = models.CharField(max_length=50, default='refugee')


class Information(models.Model):
    posted_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posted_by', null=True)
    city = models.CharField(max_length=50)
    description = models.CharField(max_length=500)


class Help(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='owner', null=True)
    slots = models.IntegerField()
    refugees = models.ManyToManyField(User, related_name='location_refugees', blank=True)
    city = models.CharField(max_length=50)
    address = models.CharField(max_length=256)
    date = models.DateTimeField()


class Requests(models.Model):
    contact = models.ForeignKey(User, on_delete=models.CASCADE, related_name='contact', null=True)
    city = models.CharField(max_length=50, null=True)
    description = models.CharField(max_length=500)


class UserNotification(models.Model):
    refugee = models.ForeignKey(User, on_delete=models.CASCADE, related_name='refugee', null=True)
    phone_number = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    status = models.CharField(max_length=50, default='inactive')
    first_name = models.CharField(max_length=50, null=True)
    last_name = models.CharField(max_length=50, null=True)

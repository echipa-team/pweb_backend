import jwt


def get_email_from_get_request(request):
    if 'access_token' not in request.GET:
        return None
    data = request.GET.get('access_token')
    try:
        email = get_email_from_access_token(data)
    except:
        return None
    return email


def get_email_from_post_request(request):
    if 'access_token' not in request.data:
        return None
    data = request.data['access_token']
    try:
        email = get_email_from_access_token(data)
    except:
        return None
    return email


def get_email_from_access_token(access_token):
    print('gatz')
    print(access_token)
    decoded_token = jwt.decode(access_token, options={"verify_signature": False})
    print(decoded_token)
    if 'https://pwebgroup.tech/email' in decoded_token:
        email = decoded_token['https://pwebgroup.tech/email']
    else:
        email = None
    return email

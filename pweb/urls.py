from django.urls import path
from .views import *

urlpatterns = [
    path('is_user/', IsUser.as_view(), name='is_user'),
    path('create_user_entry/', CreateUserEntry.as_view(), name='create_user_entry'),
    path('get_user_type/', GetUserType.as_view(), name='get_user_type'),
    path('get_locations_from_city/', GetLocationsFromCity.as_view(), name='get_locations_from_city'),
    path('get_all_locations/', GetAllHelpLocations.as_view(), name='get_all_locations'),
    path('get_my_locations/', GetMyLocations.as_view(), name='get_my_locations'),
    path('create_help_location/', CreateHelpLocation.as_view(), name='create_help_location'),
    path('join_help_location/', JoinHelpLocation.as_view(), name='join_help_location'),
    path('get_subscription_details/', GetSubscriptionDetails.as_view(), name='get_subscription_details'),
    path('update_subscription/', UpdateSubscription.as_view(), name='update_subscription'),
    path('get_all_information/', GetAllInformation.as_view(), name='get_all_information'),
    path('get_information_from_city/', GetInformationFromCity.as_view(), name='get_information_from_city'),
    path('submit_information/', SubmitInformation.as_view(), name='submit_information'),
    path('post_request/', PostRequest.as_view(), name='post_request'),
    path('delete_request/', DeleteRequest.as_view(), name='delete_request'),
    path('get_all_requests/', GetAllRequests.as_view(), name='get_all_requests'),
    path('get_my_requests/', GetMyRequests.as_view(), name='get_my_requests'),
    path('get_requests_from_city/', GetRequestsFromCity.as_view(), name='get_requests_from_city'),
]

import datetime

from django.shortcuts import render
from django.db.models import Max
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import User, Help, UserNotification, Information, Requests
from .utils import get_email_from_access_token, get_email_from_get_request, get_email_from_post_request
from django.utils import timezone
import pika
import json


class IsUser(APIView):
    def get(self, request):
        email = get_email_from_get_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        current_username = User.objects.filter(uid=email)
        if current_username.exists():
            is_user = True
        else:
            is_user = False

        return Response({"status": "success", "data": is_user})


class CreateUserEntry(APIView):
    def post(self, request):
        email = get_email_from_post_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        if 'type' not in request.data:
            return Response({"status": "error", "data": "Missing user type"})
        user_type = request.data['type']
        User.objects.create(uid=email, type=user_type)

        return Response({"status": "success"})


class GetUserType(APIView):
    def get(self, request):
        email = get_email_from_get_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        current_username = User.objects.filter(uid=email)
        if current_username.exists():
            user = User.objects.get(uid=email)
            return Response({"status": "success", "data": user.type})
        return Response({"status": "error", "data": "No associated user"})


class GetLocationsFromCity(APIView):
    def get(self, request):
        email = get_email_from_get_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        current_username = User.objects.filter(uid=email)
        if not current_username.exists():
            return Response({"status": "error", "data": "No associated user"})

        user = User.objects.get(uid=email)

        if not UserNotification.objects.filter(refugee=user).exists():
            return Response({"status": "error", "data": "Please configure city"})

        notification = UserNotification.objects.get(refugee=user)

        all_data = {"active": [], "inactive": []}

        all_locations = Help.objects.filter(city=notification.city).all()
        for current_location in all_locations:
            slots_filled = current_location.refugees.all().count()
            if current_location.date > timezone.now():
                all_data['active'].append({"contact": current_location.owner.uid,
                                           "slots": current_location.slots,
                                           "slots_filled": slots_filled,
                                           "city": current_location.city,
                                           "address": current_location.address,
                                           "date": current_location.date})
            else:
                all_data['inactive'].append({"contact": current_location.owner.uid,
                                             "slots": current_location.slots,
                                             "slots_filled": slots_filled,
                                             "city": current_location.city,
                                             "address": current_location.address,
                                             "date": current_location.date})

        return Response({"status": "success", "data": all_data})


class GetAllHelpLocations(APIView):
    def get(self, request):
        all_data = {"active": [], "inactive": []}

        all_locations = Help.objects.all()
        for current_location in all_locations:
            slots_filled = current_location.refugees.count()
            print(current_location.refugees)
            if current_location.date > timezone.now():
                all_data['active'].append({"contact": current_location.owner.uid,
                                           "slots": current_location.slots,
                                           "slots_filled": slots_filled,
                                           "city": current_location.city,
                                           "address": current_location.address,
                                           "date": current_location.date})
            else:
                all_data['inactive'].append({"contact": current_location.owner.uid,
                                             "slots": current_location.slots,
                                             "slots_filled": slots_filled,
                                             "city": current_location.city,
                                             "address": current_location.address,
                                             "date": current_location.date})

        return Response({"status": "success", "data": all_data})


class GetMyLocations(APIView):
    def get(self, request):
        email = get_email_from_get_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        current_username = User.objects.filter(uid=email)
        if not current_username.exists():
            return Response({"status": "error", "data": "No associated user"})

        user = User.objects.get(uid=email)

        all_data = {"active": [], "inactive": []}

        all_locations = Help.objects.filter(owner=user).all()
        for current_location in all_locations:
            slots_filled = current_location.refugees.all().count()
            if current_location.date > timezone.now():
                all_data['active'].append({"contact": current_location.owner.uid,
                                           "slots": current_location.slots,
                                           "slots_filled": slots_filled,
                                           "city": current_location.city,
                                           "address": current_location.address,
                                           "date": current_location.date})
            else:
                all_data['inactive'].append({"contact": current_location.owner.uid,
                                             "slots": current_location.slots,
                                             "slots_filled": slots_filled,
                                             "city": current_location.city,
                                             "address": current_location.address,
                                             "date": current_location.date})

        return Response({"status": "success", "data": all_data})


class CreateHelpLocation(APIView):
    def post(self, request):
        email = get_email_from_post_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        if not all(elem in request.data.keys() for elem in ['slots', 'city', 'address', 'date']):
            return Response({"status": "error", "data": "Missing fields"})

        user = User.objects.get(uid=email)

        Help.objects.create(owner=user,
                            slots=request.data['slots'],
                            city=request.data['city'],
                            address=request.data['address'],
                            date=timezone.make_aware(
                                datetime.datetime.strptime(request.data['date'], '%d-%m-%Y %H:%M')))

        # Also should add sending messages for notification
        phones = []
        notified_users = UserNotification.objects.filter(city=request.data['city'], status='active').all()
        for notified_user in notified_users:
            phones.append(notified_user.phone_number)

        if len(phones) > 0:
            connection = pika.BlockingConnection(
                pika.ConnectionParameters(host='167.172.186.195'))
            channel = connection.channel()

            channel.queue_declare(queue='pweb')

            message = {'phone_numbers': phones,
                       'message': f'. -\n\n'
                                  f'Hello, a new helping location was created in your city! For more details, '
                                  f'visit out website: https://pwebroup.com.\n'
                                  f'Contact for the new helping location: {user.uid}'}

            channel.basic_publish(exchange='', routing_key='pweb', body=json.dumps(message))
            print(" [x] Sent message")
            connection.close()

        return Response({"status": "success"})


class JoinHelpLocation(APIView):
    def post(self, request):
        email = get_email_from_post_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        if not all(elem in request.data.keys() for elem in ['contact', 'slots', 'city', 'address', 'date']):
            return Response({"status": "error", "data": "Missing fields"})

        user = User.objects.get(uid=email)
        owner = User.objects.get(uid=request.data['contact'])

        location = Help.objects.filter(owner=owner,
                                       slots=request.data['slots'],
                                       city=request.data['city'],
                                       address=request.data['address'],
                                       date=timezone.make_aware(
                                           datetime.datetime.strptime(request.data['date'], '%d-%m-%Y %H:%M')))
        if not location.exists():
            return Response({"status": "error", "data": "Location not found"})

        location_object = Help.objects.get(owner=owner,
                                           slots=request.data['slots'],
                                           city=request.data['city'],
                                           address=request.data['address'],
                                           date=timezone.make_aware(
                                               datetime.datetime.strptime(request.data['date'], '%d-%m-%Y %H:%M')))
        location_object.refugees.add(user)

        return Response({"status": "success"})


class GetSubscriptionDetails(APIView):
    def get(self, request):
        email = get_email_from_get_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        current_username = User.objects.filter(uid=email)
        if not current_username.exists():
            return Response({"status": "error", "data": "No associated user"})

        user = User.objects.get(uid=email)

        if not UserNotification.objects.filter(refugee=user).exists():
            all_data = {"phone_number": None,
                        "city": None,
                        "status": None,
                        "first_name": None,
                        "last_name": None}
        else:
            notification = UserNotification.objects.get(refugee=user)
            all_data = {"phone_number": notification.phone_number,
                        "city": notification.city,
                        "status": notification.status,
                        "first_name": notification.first_name,
                        "last_name": notification.last_name}

        return Response({"status": "success", "data": all_data})


class UpdateSubscription(APIView):
    def post(self, request):
        email = get_email_from_post_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        if not all(elem in request.data.keys() for elem in ['phone_number', 'city', 'status', 'first_name', 'last_name']):
            return Response({"status": "error", "data": "Missing fields"})

        user = User.objects.get(uid=email)

        if not UserNotification.objects.filter(refugee=user).exists():
            UserNotification.objects.create(refugee=user,
                                            phone_number=request.data['phone_number'],
                                            city=request.data['city'],
                                            status=request.data['status'],
                                            first_name=request.data['first_name'],
                                            last_name=request.data['last_name'])
        else:
            notification = UserNotification.objects.get(refugee=user)
            notification.phone_number = request.data['phone_number']
            notification.city = request.data['city']
            notification.status = request.data['status']
            notification.first_name = request.data['first_name']
            notification.last_name = request.data['last_name']
            notification.save()

        return Response({"status": "success"})


class GetAllInformation(APIView):
    def get(self, request):
        all_data = []

        all_information = Information.objects.all()
        for current_info in all_information:
            contact = current_info.posted_by.uid
            all_data.append({"id": current_info.id,
                             "posted_by": contact,
                             "city": current_info.city,
                             "description": current_info.description})

        return Response({"status": "success", "data": all_data})


class GetInformationFromCity(APIView):
    def get(self, request):
        email = get_email_from_get_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        current_username = User.objects.filter(uid=email)
        if not current_username.exists():
            return Response({"status": "error", "data": "No associated user"})

        user = User.objects.get(uid=email)

        if not UserNotification.objects.filter(refugee=user).exists():
            return Response({"status": "error", "data": "Please configure city"})

        notification = UserNotification.objects.get(refugee=user)

        all_data = []

        all_information = Information.objects.filter(city=notification.city).all()
        for current_info in all_information:
            contact = current_info.posted_by.uid
            all_data.append({"id": current_info.id,
                             "posted_by": contact,
                             "city": current_info.city,
                             "description": current_info.description})

        return Response({"status": "success", "data": all_data})


class SubmitInformation(APIView):
    def post(self, request):
        email = get_email_from_post_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        if not all(elem in request.data.keys() for elem in ['city', 'description']):
            return Response({"status": "error", "data": "Missing fields"})

        user = User.objects.get(uid=email)

        if user.type != "admin":
            return Response({"status": "error", "data": "Not admin!"})

        Information.objects.create(posted_by=user,
                                   city=request.data['city'],
                                   description=request.data['description'])

        return Response({"status": "success"})


class PostRequest(APIView):
    def post(self, request):
        email = get_email_from_post_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        if not all(elem in request.data.keys() for elem in ['city', 'description']):
            return Response({"status": "error", "data": "Missing fields"})

        user = User.objects.get(uid=email)

        if user.type != "refugee":
            return Response({"status": "error", "data": "Not refugee!"})

        Requests.objects.create(contact=user,
                                city=request.data['city'],
                                description=request.data['description'])

        return Response({"status": "success"})


class DeleteRequest(APIView):
    def post(self, request):
        email = get_email_from_post_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        if not all(elem in request.data.keys() for elem in ['id']):
            return Response({"status": "error", "data": "Missing fields"})

        user = User.objects.get(uid=email)

        if user.type != "refugee":
            return Response({"status": "error", "data": "Not refugee!"})

        if Requests.objects.filter(id=request.data['id'], contact=user).exists():
            Requests.objects.filter(id=request.data['id'], contact=user).delete()
            return Response({"status": "success"})

        return Response({"status": "error", "data": "No match found"})


class GetAllRequests(APIView):
    def get(self, request):
        all_data = []

        all_requests = Requests.objects.all()
        for current_info in all_requests:
            contact = current_info.contact.uid
            all_data.append({"id": current_info.id,
                             "posted_by": contact,
                             "city": current_info.city,
                             "description": current_info.description})

        return Response({"status": "success", "data": all_data})


class GetMyRequests(APIView):
    def get(self, request):
        email = get_email_from_get_request(request)
        if email is None:
            return Response({"status": "error", "data": "Invalid or missing access token"})

        user = User.objects.get(uid=email)

        all_data = []

        all_requests = Requests.objects.filter(contact=user)
        for current_info in all_requests:
            contact = current_info.contact.uid
            all_data.append({"id": current_info.id,
                             "posted_by": contact,
                             "city": current_info.city,
                             "description": current_info.description})

        return Response({"status": "success", "data": all_data})


class GetRequestsFromCity(APIView):
    def get(self, request):
        if not all(elem in request.GET.keys() for elem in ['city']):
            return Response({"status": "error", "data": "Missing fields"})

        all_data = []

        all_requests = Requests.objects.filter(city=request.GET['city']).all()
        for current_info in all_requests:
            contact = current_info.contact.uid
            all_data.append({"id": current_info.id,
                             "posted_by": contact,
                             "city": current_info.city,
                             "description": current_info.description})

        return Response({"status": "success", "data": all_data})

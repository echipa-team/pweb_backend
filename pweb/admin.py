from django.contrib import admin

# Register your models here.
from .models import User, Help, UserNotification, Information, Requests

admin.site.register(User)
admin.site.register(Help)
admin.site.register(UserNotification)
admin.site.register(Information)
admin.site.register(Requests)

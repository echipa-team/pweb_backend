FROM python:3.8.3-slim-buster
RUN apt-get update \
    && apt-get -y install libpq-dev gcc \
    && pip install psycopg2 djangorestframework django-sslserver pyjwt pika django-cors-headers

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /src
COPY requirements.txt /src
RUN pip install -r requirements.txt
COPY /backend /src/backend
COPY /cert /src/cert
COPY /pweb /src/pweb
COPY manage.py /src
